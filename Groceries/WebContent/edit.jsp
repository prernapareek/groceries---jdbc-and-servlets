<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.*,com.ts.controller.*,com.ts.dao.*,com.ts.dto.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Online Groceries Store</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color: white;">
<script type="text/javascript">
		function evalGroup() {
			var group = document.radioForm.id;
			for (var i = 0; i < group.length; i++) {
				if (group[i].checked)
					break;
			}
			if (i == group.length)
				return alert("No radio button is checked");
			alert("Radio Button " + (i + 1) + " is checked.");
		}
	</script>
​
	<form name = "radioForm" action = "GroceriesController" method="get">
		<div class="container">
			<br> <br>
			<center>
				<u><h2>Groceries</h2></u>
			</center>
			<br>
			<table class="table" border='0px'>
				<thead>
					<tr>
					    <th>ProductId</th>
						<th>ProductName</th>
						<th>Size</th>
						<th>UnitPrice</th>

					</tr>
				</thead>

				<tbody>
					<c:forEach items="${gList}" var="glist">

						<tr>
						    <td><input type="radio" name="id"  method="post" value="${glist.id}"></td>
							<td>${glist.name}</td>
							<td>${glist.size}</td>
							<td>${glist.price}</td>

						</tr>

					</c:forEach>

				</tbody>
			</table>
			<br> <br>
			<center>
			<tr><td colspan="2"><button type="submit" name="action"  value="Update" onclick = "evalGroup()">Update</button></td></tr>
			
			 
			<tr><td colspan="2"><button type="submit" name="action" value="Delete" onclick = "evalGroup()">Delete</button></td></tr> 
			
			
			<tr><td colspan="2"><button type="submit" name="action" value="Home">Home</button></td></tr> 
			
			</center>
		</div>
	</form>
</body>
</html>