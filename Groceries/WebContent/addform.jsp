<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Form</title>
</head>

<body>
<script type="text/javascript">
function validateform(){
	var name=document.addform.name.value;
	var size=document.addform.size.value;
	var price=document.addform.price.value;
	
	if(name==null || name==""){
		alert("Name can't be blank ");
		return false;
	}
	else if(size==null || size==""){
		alert("Size can't be blank ");
		return false;
	}
	else if(price==null || price==""){
		alert("Price can't be blank ");
		return false;
	}
	
}
</script>
<h1>Add New Items</h1>  
<form name = "addform" action="AddServlet" method="post" onsubmit = "return validateform()">  
<table>  
 
<tr><td>Name:</td><td><input type="text" name="name"/></td></tr>  
<tr><td>Size:</td><td><input type="text" name="size"/></td></tr>  
<tr><td>Price:</td><td><input type="text" name="price"/></td></tr>  

<tr><td colspan="2"><input type="submit" value="Add Items"/></td></tr>  
</table>  
</form>  

</body>
</html>