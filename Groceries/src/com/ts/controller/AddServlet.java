package com.ts.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.ts.dao.GroceriesDao;

import com.ts.dto.Groceries;




@WebServlet("/AddServlet")
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
          
        String name=request.getParameter("name");  
        String size=request.getParameter("size");  
        int price=Integer.parseInt(request.getParameter("price"));
        
          
      Groceries g =new Groceries();  
        g.setName(name);  
        g.setSize(size); 
        g.setPrice(price);
          
        int status=GroceriesDao.save(g);  
        if(status>0){ 
        	out.print("<p><h1>Items updated successfully!</h1></p>");  
			 response.sendRedirect("GroceriesController"); 
        	//request.setAttribute("message", "Items Added");
            //request.getRequestDispatcher("GroceriesController").include(request, response);
        }else{  
            out.println("Sorry! try again.....");  
        }  
          
        out.close();  
    }  
}


