package com.ts.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ts.dao.GroceriesDao;
import com.ts.dto.Groceries;

@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		/*String sid = request.getParameter("id");
		int id = Integer.parseInt(sid);*/
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String size = request.getParameter("size");
		int price = Integer.parseInt(request.getParameter("price"));

		Groceries g = new Groceries();
		g.setId(id);
		g.setName(name);
		g.setSize(size);
		g.setPrice(price);

		int status = GroceriesDao.update(g);
		if (status > 0) {
			 out.print("<p><h1>Items updated successfully!</h1></p>");  
			 response.sendRedirect("GroceriesController"); 
			
	         
			
		} else {
			out.println("Sorry! unable to update record");
		}

		out.close();
	}

}
