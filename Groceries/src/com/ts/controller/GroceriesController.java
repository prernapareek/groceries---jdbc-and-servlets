package com.ts.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;
import com.ts.dao.DaoUtility;
import com.ts.dao.GroceriesDao;
import com.ts.dto.Groceries;

@WebServlet("/GroceriesController")
public class GroceriesController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GroceriesController() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");

		if (action == null) {
			try {
				List<Groceries> list = GroceriesDao.getGroceriesList();
				System.out.println(list.size());
				request.setAttribute("gList", list);
				System.out.println("do get method");
				request.getRequestDispatcher("view.jsp").forward(request, response);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e);
			}

		}
		if (action.equals("AddItems")) {
			try {

				request.getRequestDispatcher("addform.jsp").forward(request, response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e);
			}
		}

		if (action.equals("EditItems")) {
			try {
				List<Groceries> list = GroceriesDao.getGroceriesList();
				System.out.println(list.size());
				request.setAttribute("gList", list);
				System.out.println("do get method");
				request.getRequestDispatcher("edit.jsp").forward(request, response);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e);
			}

		}
		if (action.equals("Update")) {
			try {
				List<Groceries> list = GroceriesDao.getGroceriesList();
				System.out.println(list.size());
				request.setAttribute("gList", list);
				System.out.println("do get method");
				request.getRequestDispatcher("EditServlet").forward(request, response);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e);
			}

		}

		if (action.equals("Delete")) {
			try {
				List<Groceries> list = GroceriesDao.getGroceriesList();
				System.out.println(list.size());
				request.setAttribute("gList", list);
				System.out.println("do get method");
				request.getRequestDispatcher("DeleteServlet").forward(request, response);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e);
			}

		}
		
		if (action.equals("Home")) {
			try {
				List<Groceries> list = GroceriesDao.getGroceriesList();
				System.out.println(list.size());
				request.setAttribute("gList", list);
				System.out.println("do get method");
				response.sendRedirect("GroceriesController");
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e);
			}

		}
		
		
		

	}
}
