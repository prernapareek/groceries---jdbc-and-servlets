package com.ts.controller;

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ts.dao.GroceriesDao;
import com.ts.dto.Groceries;

@WebServlet("/EditServlet")

public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<h1>Update Items</h1>");
		int id=Integer.parseInt(request.getParameter("id"));
		

		Groceries g = GroceriesDao.getGroceriesId(id);

		out.print("<form action ='UpdateServlet' method = 'post'>");

	    out.print("<table>");
	    out.print("<tr><td></td><td><input type='hidden' name='id' value='"+g.getId()+"'/></td></tr>");  
		out.print("<tr><td>Name:</td><td><input type='text' name='name' value='" + g.getName() + "'/></td></tr>");
		out.print("<tr><td>Size:</td><td><input type='text' name='size' value='" + g.getSize() + "'/></td></tr>");
		out.print("<tr><td>Price:</td><td><input type='number' name='price' value='" + g.getPrice() + "'/></td></tr>");
		out.print("<tr><td colspan='2'><button type='submit' name='action' value='savechanges'>SAVE CHANGES</button></td></tr>");
		out.print("</table>");
		out.print("</form>");

		out.close();
	}
}
